package com.mobile.ict.cart;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.View;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends Activity {


    EditText emailId,firstPassword,confirmPassword,address,name;
    String memberNumber,response;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    SignUpTask signUpTask;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        sharedPref= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit();

        response = getIntent().getExtras().getString("json");
        memberNumber = getIntent().getExtras().getString("mobilenumber");

        registerViews();
    }

    private void registerViews() {


        name = (EditText) findViewById(R.id.name);
        name.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        



        emailId = (EditText) findViewById(R.id.emailID);
        emailId.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        
        







        firstPassword = (EditText) findViewById(R.id.firstpassword);
        firstPassword.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        
        
        



        confirmPassword = (EditText) findViewById(R.id.confirmpassword);
        confirmPassword.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        

        address = (EditText) findViewById(R.id.address);
        address.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });
 

        

    }










    public void onSubmit(View v)
    {

        if ( checkValidation () )
            submitForm();
        else
            Toast.makeText(SignUpActivity.this,R.string.label_toast_Form_contains_error, Toast.LENGTH_LONG).show();
    }

    private void submitForm() {


        JSONObject signupObj = new JSONObject();



        try {
            signupObj.put("name",name.getText().toString());
            signupObj.put("email",emailId.getText().toString());
            signupObj.put("password",firstPassword.getText().toString());
            signupObj.put("address", address.getText().toString());
            signupObj.put("phonenumber", memberNumber);
            if(connect()){
                signUpTask = new SignUpTask(SignUpActivity.this);
                signUpTask.execute(signupObj);
            }
            else{
                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }





    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(this,name)) ret = false;
        if (!Validation.isEmailAddress(this,emailId, true,"emailid")) ret = false;
        if (!Validation.hasText(this,firstPassword)) ret = false;
        if (!Validation.isPasswordMatch(this,confirmPassword,firstPassword.getText().toString(),true)) ret = false;
        if (!Validation.hasText(this,address)) ret = false;
        return ret;
    }










    public class  SignUpTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public SignUpTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {


            String url ="http://ruralict.cse.iitb.ac.in/RuralIvrs/app/register";



            JsonParser jParser = new JsonParser();
            response = jParser.getJSONFromUrl(url,params[0],"POST",false,null,null);
            return response;
        }





        @Override
        protected void onPostExecute(String response1) {

            pd.dismiss();



            if(response1.equals("exception"))
            {
                    Bundle args = new Bundle();
                args.putInt("msg",1);


                myDialogFragment ob2=new myDialogFragment();


                FragmentManager fm=getFragmentManager();
                ob2.setArguments(args);
                ob2.show(fm, "1");



            }
            else
            {


                try {
                    JSONObject resObj = new JSONObject(response1);

                    if(resObj.getString("Status").equals("Success")) {

                        Bundle args = new Bundle();
                        args.putInt("msg",2);


                        myDialogFragment ob2=new myDialogFragment();


                        FragmentManager fm=getFragmentManager();
                        ob2.setArguments(args);
                        ob2.setCancelable(false);
                        ob2.show(fm, "2");


                    }
                    else {
                        Toast.makeText(getApplicationContext(), resObj.getString("Error")+"Sorry, please try again later", Toast.LENGTH_SHORT).show();

                        if(resObj.getString("Error").equals("Email Exists"))
                            emailId.setError("Email already exists");


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }





        }
    }


    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder;
            int msg = getArguments().getInt("msg");


            switch(msg)
            {
                case 1 :    builder = new AlertDialog.Builder(getActivity());
                              builder.setMessage(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                              .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                          public void onClick(DialogInterface dialog, int id) {


                                              finish();

                                          }
                                      }
                              );
                                return builder.create();


                case 2 :    builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(R.string.label_alertdialog_Your_registration_form_submitted_successfully)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            finish();

                                        }
                                    }
                            );
                                  return builder.create();


                default:return  null;

            }



        }


    }


    public boolean connect(){
        String connection= "false";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){

            connection="true";
        }
        Boolean con = Boolean.valueOf(connection);
        return con;

    }






}
