package com.mobile.ict.cart;

 import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class HelpActivity extends Activity {
    ListView listView ;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_list);
        listView = (ListView) findViewById(R.id.list);
        
        String[] values = new String[]{getResources().getString(R.string.label_activity_Help_About_Us),
                getResources().getString(R.string.label_activity_Help_Contact_Us),
                getResources().getString(R.string.label_activity_Help_More)
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_1, android.R.id.text1, values);

        listView.setAdapter(adapter);
        
        listView.setOnItemClickListener(new OnItemClickListener() {

              @Override
              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
               int itemPosition     = position;


               
               String  itemValue    = (String) listView.getItemAtPosition(position);



              if(itemValue.equals(getResources().getString(R.string.label_activity_Help_Contact_Us))){
                	Uri uriUrl = Uri.parse("http://ruralict.cse.iitb.ac.in");
                	Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                	startActivity(launchBrowser);
                } else if(itemValue.equals(getResources().getString(R.string.label_activity_Help_About_Us))){
                	Intent intent = new Intent(HelpActivity.this,AboutUsActivity.class);
                	startActivity(intent);
                } 
                else{
                  Intent intent = new Intent(HelpActivity.this,UserManualActivity.class);
                  startActivity(intent);

                }
             
              }

         }); 
    }

}
