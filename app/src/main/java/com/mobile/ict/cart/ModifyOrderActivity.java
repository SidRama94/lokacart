package com.mobile.ict.cart;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import android.preference.PreferenceManager;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


@SuppressLint("NewApi")
public class ModifyOrderActivity extends Activity implements OnClickListener {

    Button b1;
    EditText quantity;
    JSONObject order;
    TextView heading;
    double grandTotal;
    Bundle bundle;
    Map <String,String> itemsMap = new HashMap<String, String>();
    private double Total=0.0;
    ListView list;
    ModifyOrderRowAdapter adapter;
    ArrayList<ItemRow> itemss;
    ArrayList<OrderRow> items;
    String memberNumber,orgAbbr,orgId;
    SharedPreferences sharedPref;
    int orderId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_order);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);

        heading=(TextView)findViewById(R.id.label_choose_order);
        b1=(Button)findViewById(R.id.button_proceed_confirm);


        heading.setTypeface(tf1);
        b1.setTypeface(tf1);

        b1.setOnClickListener(this);

        bundle = new Bundle();

        list = (ListView) findViewById(R.id.new_order_item_list);


        itemsMap = (Map)getIntent().getSerializableExtra("itemsMap");
        bundle = getIntent().getExtras();

        List<OrderRow> order_item_list = getItems();

        adapter = new ModifyOrderRowAdapter(ModifyOrderActivity.this, R.layout.order_row, order_item_list,bundle);
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();



    }

    public List<OrderRow> getItems() {

        items = new ArrayList<OrderRow>();

        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
        for(Entry<String, String> entry : sortList.entrySet()){
            OrderRow item = new OrderRow(entry.getKey(),Double.parseDouble(entry.getValue()),0.0);
            items.add(item);
        }
        return items;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);

        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }
    @Override
    public void onClick(View view) {

        if(view == b1){



            itemss = new ArrayList<>();

            Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
            Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
            for(Map.Entry<String, String> entry : sortList.entrySet()){
                double quantity = bundle.getDouble(entry.getKey(),0);
                if(quantity > 0) {
                    double total = quantity * Double.parseDouble(entry.getValue());
                    Total += total;
                    ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                    itemss.add(item);
                }
            }

            if(Total<=0){


                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,
                        Toast.LENGTH_LONG).show();
            }
            else {

                try {
                    order = new JSONObject();
                    order.put("orgabbr",sharedPref.getString("orgAbbr",""));
                    order.put("groupname", "Parent Group");
                    order.put("status","saved");
                    JSONArray products = new JSONArray();
                    JSONObject object;
                    int i=1;
                    for(ItemRow item : itemss)
                    {
                        object  = new JSONObject();
                        object.put("name",item.getName());
                        object.put("quantity",item.getQuantity());
                        products.put(object);
                        i++;
                    }
                    order.put("orderItems",products);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                orderId=getIntent().getExtras().getInt("orderId");
                new WriteToServer(ModifyOrderActivity.this,orderId).execute(order);


            }


        }


    }



    public class WriteToServer extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;
        String val;
        int id;

        public WriteToServer(Context context,int id){
            this.context=context;
            this.id=id;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //lockScreenOrientation();
            pd = new ProgressDialog(ModifyOrderActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        protected String doInBackground(JSONObject... params) {

            String serverUrl="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/update/"+id;
            JsonParser jParser = new JsonParser();
            String response = jParser.getJSONFromUrl(serverUrl,params[0],"POST",true,sharedPref.getString("emailid",null),sharedPref.getString("password",null));

            return response;

        }

        protected void onPostExecute(String response1) {

            pd.dismiss();

            if(response1.equals("exception"))
            {
                Toast.makeText(context,R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later, Toast.LENGTH_SHORT).show();
            }
            else {
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(response1);
                    val=jsonObj.getString("Status");

                    if(val.equals("Success"))
                    {
                        Toast.makeText(context,R.string.label_toast_Your_order_has_been_updated_successfully, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ModifyOrderActivity.this,CancelOrderActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finishModifyOrderActivity();


                    }

                    else

                        Toast.makeText(context,R.string.label_toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }


            //unlockScreenOrientation();


        }



        private void lockScreenOrientation() {
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        private void unlockScreenOrientation() {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }

    }

    public void finishModifyOrderActivity() {
        ModifyOrderActivity.this.finish();
    }

}
