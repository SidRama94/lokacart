package com.mobile.ict.cart;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class DraftOrderAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<DraftOrder> objects;
    String symbol = "" + '\u20B9';
    DraftOrderAdapter(Context context, ArrayList<DraftOrder> draftOrder) {
        ctx = context;
        objects = draftOrder;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.draft_item, parent, false);
        }

        DraftOrder p = getProduct(position);

        ((TextView) view.findViewById(R.id.quantityName)).setText(p.quantityName);
        ((TextView) view.findViewById(R.id.quantity)).setText(p.quantity);
        ((TextView) view.findViewById(R.id.quantityBasePrice)).setText(p.quantityBasePrice);
        ((TextView) view.findViewById(R.id.quantityTotalPrice)).setText(p.quantityTotalPrice);

        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.quantityCheckBox);
        if(ctx instanceof DraftToOrder)
        {
            cbBuy.setVisibility(View.GONE);


        }
        else
        {

            cbBuy.setOnCheckedChangeListener(myCheckChangList);
            cbBuy.setTag(position);
            cbBuy.setChecked(p.box);

        }

        return view;
    }

    DraftOrder getProduct(int position) {
        return ((DraftOrder) getItem(position));
    }

    ArrayList<DraftOrder> getBox() {
        ArrayList<DraftOrder> box = new ArrayList<DraftOrder>();
        for (DraftOrder p : objects) {
            {
                box.add(p);

            }

        }
        return box;
    }

    OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).box = isChecked;
            getProduct((Integer) buttonView.getTag()).position=(Integer) buttonView.getTag();
        }
    };
}

