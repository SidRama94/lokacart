package com.mobile.ict.cart;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import java.util.ArrayList;


public class OrdersDraftDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Order_drafts.db";
    public static final String TABLE_NAME = "draftss";

    public final class OrdersEntry implements BaseColumns{
        
        public static final String TABLE_NAME = "draftss";
        public static final String COLUMN_NAME_ORG_ID = "org_id";
        public static final String COLUMN_NAME_ORGABBR = "org_abbr";
        public static final String COLUMN_NAME_DRAFT_ID = "draft_id";
        public static final String COLUMN_NAME_PRODUCT_NAME = "product_name";
        public static final String COLUMN_NAME_MEMBER_NUMBER = "member_number";
        public static final String COLUMN_NAME_PRICE = "price";
        public static final String COLUMN_NAME_TOTAL = "total";
        public static final String COLUMN_NAME_QUANTITY = "quantity";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

    }

    

    public OrdersDraftDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
    	db.execSQL("CREATE TABLE " + OrdersEntry.TABLE_NAME + " (" +
                OrdersEntry.COLUMN_NAME_ORG_ID + " TEXT NOT NULL," +
                OrdersEntry.COLUMN_NAME_ORGABBR + " TEXT NOT NULL, " +
                OrdersEntry.COLUMN_NAME_DRAFT_ID + " INTEGER NOT NULL," +
                OrdersEntry.COLUMN_NAME_PRODUCT_NAME + " TEXT NOT NULL , " +
                OrdersEntry.COLUMN_NAME_PRICE + " INTEGER NOT NULL," +
                OrdersEntry.COLUMN_NAME_TOTAL + " INTEGER NOT NULL," +
                OrdersEntry.COLUMN_NAME_QUANTITY + " INTEGER NOT NULL," +
                OrdersEntry.COLUMN_NAME_MEMBER_NUMBER + " TEXT NOT NULL, " +
                OrdersEntry.COLUMN_NAME_TIMESTAMP + " TEXT NOT NULL " +
                ")");
    
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Tasks to perform On Upgrde
        db.execSQL(" DROP TABLE IF EXISTS " + OrdersEntry.TABLE_NAME);
    	onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

  
    public long insertOrder(double unitPrice, double quantity ,double total, String name,String memberNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_PRODUCT_NAME, name);
        values.put(OrdersEntry.COLUMN_NAME_PRICE, unitPrice);
        values.put(OrdersEntry.COLUMN_NAME_TOTAL, total);
        values.put(OrdersEntry.COLUMN_NAME_MEMBER_NUMBER, memberNumber);
        values.put(OrdersEntry.COLUMN_NAME_QUANTITY, quantity);

        long newRowID = db.insert(OrdersEntry.TABLE_NAME, null, values);

        db.close();
        return newRowID;
    }

    public long insertDraftOrder(String org_id,String orgAbbr,int draft_id,double unitPrice, double quantity ,double total, String name,String memberNumber, String timestamp) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_ORG_ID, org_id);
        values.put(OrdersEntry.COLUMN_NAME_ORGABBR, orgAbbr);
        values.put(OrdersEntry.COLUMN_NAME_DRAFT_ID, draft_id);
        values.put(OrdersEntry.COLUMN_NAME_PRODUCT_NAME, name);
        values.put(OrdersEntry.COLUMN_NAME_PRICE, unitPrice);
        values.put(OrdersEntry.COLUMN_NAME_TOTAL, total);
        values.put(OrdersEntry.COLUMN_NAME_QUANTITY, quantity);
        values.put(OrdersEntry.COLUMN_NAME_MEMBER_NUMBER, memberNumber);
        values.put(OrdersEntry.COLUMN_NAME_TIMESTAMP, timestamp);

        long newRowID = db.insert(OrdersEntry.TABLE_NAME, null, values);


        db.close();
        return newRowID;
    }


    public void deleteOrder( String name,double quantity ) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_PRODUCT_NAME, name);

        values.put(OrdersEntry.COLUMN_NAME_QUANTITY, quantity);

        long newRowID = db.delete(TABLE_NAME, OrdersEntry.COLUMN_NAME_PRODUCT_NAME + "= ?" + " AND " + OrdersEntry.COLUMN_NAME_QUANTITY + "=  ?", new String[]{name, String.valueOf(quantity)});
        db.close();

    }


    public void deleteDraftOrder(String draft_id , String orgAbbr,String orgId,String memberNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_DRAFT_ID,draft_id);

        long newRowID = db.delete(TABLE_NAME, OrdersEntry.COLUMN_NAME_DRAFT_ID + "= ?"+" and "+OrdersEntry.COLUMN_NAME_ORGABBR + "= ?"+" and "+OrdersEntry.COLUMN_NAME_ORG_ID+ "= ?"+" and "+OrdersEntry.COLUMN_NAME_MEMBER_NUMBER+ "= ?" , new String[]{draft_id,orgAbbr,orgId,memberNumber});

        db.close();

    }


    public ArrayList<ArrayList<String[]>> selectdraftOrder(String orgAbbr,String orgId,String memberNumber) {

        String item[] = new String[7];
        ArrayList<String[]> draft_item;

        ArrayList<ArrayList<String[]>> list = new ArrayList<ArrayList<String[]>>();

        SQLiteDatabase db = this.getWritableDatabase();


        Cursor id_cursor = db.rawQuery("select distinct draft_id from draftss where org_abbr = " +"\""+orgAbbr+"\""+" and org_id = " +"\""+orgId+"\""+" and member_number = " +"\""+memberNumber+"\"" ,null);




        if(id_cursor.getCount()!=0)
        {

            while (id_cursor.moveToNext()) {


                Cursor draft_cursor = db.rawQuery("select * from draftss where draft_id=" + id_cursor.getString(0)+" and org_abbr = " +"\""+orgAbbr+"\""+" and org_id = " +"\""+orgId+"\""+" and member_number = " +"\""+memberNumber+"\"", null);


                draft_item = new ArrayList<String[]>();

                while (draft_cursor.moveToNext()) {


                    item = new String[7];
                    item[0]=draft_cursor.getString(2);
                    item[1]=draft_cursor.getString(3);
                    item[2]=draft_cursor.getString(4);
                    item[3]=draft_cursor.getString(5);
                    item[4]=draft_cursor.getString(6);
                    item[5]=draft_cursor.getString(7);
                    item[6]=draft_cursor.getString(8);

                    draft_item.add(item);

                }

                list.add(draft_item);

                draft_cursor.close();

            }

        }

        id_cursor.close();

        db.close();
        return list;
    }



    public ArrayList<String[]> selectDraftOrderCount(String orgAbbr,String orgId)
    {

        System.out.println("-----------------selecting record------------------");

        String item[];
        ArrayList<String[]> product_list= new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select distinct(product_name),price,quantity,draft_id from draftss where org_abbr = " + "\""+orgAbbr+"\"" + " and org_id = " + "\""+orgId+"\"", null);
        while (cursor.moveToNext())
        {
            item = new String[4];
            item[0]= cursor.getString(0);
            item[1]=cursor.getString(1);
            item[2]=cursor.getString(2);
            item[3]=cursor.getString(3);

            System.out.println(orgAbbr+"------------"+orgId+"---------------"+item[0]+"-----------------" + item[1] + "-----------------"+item[2] + "--------------"+item[3]);

            product_list.add(item);

        }

        db.close();
        return product_list;


    }



    public void updateDraftOrder(String orgAbbr,String orgId,String productName,double price,double quantity,int draftId)
    {
        System.out.println("-----------------updating record------------------");
        SQLiteDatabase db = this.getWritableDatabase();

        double total=0.0;

        total = price * quantity;

        String WHERE =  OrdersEntry.COLUMN_NAME_ORGABBR + "= ?" +  " and "+OrdersEntry.COLUMN_NAME_ORG_ID + "= ?" + " and "+OrdersEntry.COLUMN_NAME_PRODUCT_NAME + "= ?"+" and "+OrdersEntry.COLUMN_NAME_DRAFT_ID + "= "+draftId ;

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_PRICE,price);
        values.put(OrdersEntry.COLUMN_NAME_TOTAL,total);


        db.update(TABLE_NAME, values, WHERE, new String[]{orgAbbr,orgId, productName});

        db.close();
    }



    public int selectMaxDraftOrderID(String orgAbbr,String orgId,String memberNumber)
    {
        int count=0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select  max (draft_id) from draftss where org_abbr = " + "\""+orgAbbr+"\"" + " and org_id = " + "\""+orgId+"\""+" and member_number = " +"\""+memberNumber+"\"", null);


        while (cursor.moveToNext())
        {
            count=cursor.getInt(0);

        }
            db.close();

        return count;


    }


}
